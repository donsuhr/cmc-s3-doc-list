import $ from 'jquery';
import api from '../../src/main';
import './page.scss';
import template from './testTemplate.hbs';

const $el = $('#s3docListApp');

api.init({
    $el,
    site: $el.data('site'),
    group: $el.data('group'),
    docType: $el.data('doctype'),
    menuText: $el.data('menu-text'),
    template,
});
