// write front-end SAFE variables to config file
const writeConfig = require('cmc-write-config'); // eslint-disable-line import/no-extraneous-dependencies
const path = require('path');

const outputPath = path.resolve(__dirname, '../');
writeConfig({ outputPath });
