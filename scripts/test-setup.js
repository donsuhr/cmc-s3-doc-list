// const nock = require('nock');
require('babel-core/register'); // eslint-disable-line import/no-extraneous-dependencies
// require('babel-polyfill');
// require('core-js/fn/object/values');

const jsdom = require('jsdom-global'); // eslint-disable-line import/no-extraneous-dependencies
const chai = require('chai'); // eslint-disable-line import/no-extraneous-dependencies
const dirtyChai = require('dirty-chai'); // eslint-disable-line import/no-extraneous-dependencies
const sinonChai = require('sinon-chai'); // eslint-disable-line import/no-extraneous-dependencies

chai.use(dirtyChai);
chai.use(sinonChai);

jsdom(undefined, { url: 'https://api.suhrthing.com:9000' });
global.self = global;
const $ = require('jquery');
require('whatwg-fetch'); // eslint-disable-line import/no-extraneous-dependencies

global.$ = $;
global.jQuery = $;
window.jQuery = $;

// nock.recorder.rec();
