import Backbone from 'backbone';
import _ from 'underscore';
import $ from 'jquery';

export default Backbone.Model.extend({
    defaults: {
        Product: '',
    },
    setState(options) {
        _.map(options, (value, key) => {
            if (this.has(key)) {
                this.set(key, $.trim(value));
            }
        });
    },
});
