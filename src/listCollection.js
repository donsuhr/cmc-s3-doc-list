import Backbone from 'backbone';
import _ from 'underscore';
import config from 'config';
import Model from './s3ItemModel';

const ListCollection = Backbone.Collection.extend({
    model: Model,
    url() {
        return `${config.domains.api}/${this.docType}/${this.site}/${this
            .group}/`;
    },
    comparator(item) {
        return -item.get('priority');
    },
    initialize(models, options) {
        this.site = options.site;
        this.group = options.group;
        this.docType = options.docType;
        this.s3BucketPrefix = config.s3BucketPrefix;
        this.options = options;
    },
    parse(response) {
        return response.data;
    },

    applyFilter(filter) {
        const clonedFilter = _.clone(filter.attributes);
        _.each(clonedFilter, (value, key) => {
            if (!value) {
                delete clonedFilter[key];
            }
        });
        let selectedProduct;
        if (clonedFilter.Product) {
            selectedProduct = clonedFilter.Product;
            delete clonedFilter.Product;
        }
        let result = _.keys(clonedFilter).length
            ? this.where(clonedFilter)
            : this.models;
        if (selectedProduct) {
            result = _.filter(result, (value, index, collection) =>
                _.contains(value.get('products'), selectedProduct));
        }
        return new ListCollection(result, this.options);
    },

    getUniqueProducts() {
        return _.unique(
            _.filter(_.flatten(this.pluck('products'), x => x !== '')),
        ).sort();
    },
});

export default ListCollection;
