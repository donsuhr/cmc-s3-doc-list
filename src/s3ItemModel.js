/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */

import Backbone from 'backbone';
import config from 'config';

export default Backbone.Model.extend({
    parse(response) {
        // /docs/:type/:site/:group/:file
        // https://cmcv3-:type--:site--:group.s3.amazonaws.com/:file   200
        response.fullUrl = process.env.NODE_ENV === 'production'
            ? `${config.domains.self}/docs/${this.collection.docType}/${this.collection.site}/${this.collection.group}/${response.s3url}`
            : `https://${this.collection.s3BucketPrefix}${this.collection.docType}--${this.collection.site}--${this.collection.group}.s3.amazonaws.com/${response.s3url}`;

        if (this.collection.docType === 'case-studies') {
            const langPrefix = this.collection.group === 'en-us' ? '' : `${this.collection.group}/`;
            // response.fullUrl = langPrefix === 'pr-br/' ?
            //     `${config.domains.self}/${langPrefix}solucoes/
            //          dl-case-study/${response._id}/${response.s3url}/`
            //     : `${config.domains.self}/${langPrefix}higher-education-resources/
            //          dl-case-study/${response._id}/${response.s3url}/`;
            response.fullUrl = langPrefix === 'pt-br/' ?
                `/solucoes/dl-case-study/${response._id}/${response.s3url}/`
                : `/${langPrefix}higher-education-resources/dl-case-study/${response._id}/${response.s3url}/`;
        }

        return response;
    },
});
