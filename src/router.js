import Backbone from 'backbone';
import _ from 'underscore';
import $ from 'jquery';

export default Backbone.Router.extend({
    routes: {
        'filter(/Product/:product)': 'filter',
    },
    filter(product, category, type) {
        this.trigger('route:filter', {
            Product: product,
        });
    },
    updateFilter(filter) {
        let url = 'filter';
        _.map(filter.attributes, (value, key) => {
            if ($.trim(value) !== '') {
                url += `/${key}/${value}`;
            }
        });
        this.navigate(url, { trigger: false });
    },
});
