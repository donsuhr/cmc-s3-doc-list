/* globals s3DocListPreloadData */
/* eslint-disable no-new */

import Backbone from 'backbone';
import AppView from './views/appView';
import Router from './router';

const api = {
    init(options) {
        const router = new Router();
        const preloadData = typeof s3DocListPreloadData !== 'undefined' ?
            s3DocListPreloadData : [];
        new AppView({
            el: options.$el,
            router,
            preloadData,
            docType: options.docType,
            site: options.site,
            group: options.group,
            menuText: options.menuText || 'Select Product',
            template: options.template,
        });
        Backbone.history.start();
    },
};

export default api;
