import Backbone from 'backbone';
import _ from 'underscore';
import SubMenu from 'cmc-horizontal-menu-item-view';

require('superfish');

const superFishOptions = {
    cssArrows: false,
    speed: 'fast',
    animation: { height: 'show' },
    animationOut: { height: 'hide' },
};

export default Backbone.View.extend({
    submenus: {},
    initialize(options) {
        this.listenTo(this.model.get('filter'), 'change', this.onFilterChange);
        this.listenTo(this.model.get('allItemsCollection'), 'request', this.renderReset);
        this.listenTo(this.model.get('allItemsCollection'), 'sync reset', this.updateMenuItemData);
        this.listenTo(Backbone, 'horizontalMenuItem:change', this.onMenuChange);
        this.menuText = options.menuText || 'Select Product';
        this.submenus = {};
        this.render();
    },

    renderReset() {
        this.$el.superfish('destroy');
        _.map(this.submenus, (value, key) => {
            value.setItems([]);
        });
    },

    updateMenuItemData() {
        const items = this.model.get('allItemsCollection');
        this.submenus.productsMenu.setItems(items.getUniqueProducts());
        this.$el.superfish('destroy').superfish(superFishOptions);
        this.onFilterChange();
    },

    render(event) {
        this.submenus.productsMenu = new SubMenu({
            items: [],
            property: 'Product',
            unselectedText: this.menuText,
        });
        this.$el.append(this.submenus.productsMenu.el);
    },

    onFilterChange(event) {
        const filter = this.model.get('filter');
        this.submenus.productsMenu.setSelectedText(filter.get('Product'));
    },

    onMenuChange(event) {
        this.model.get('filter').set(event.property, event.value);
    },
});
