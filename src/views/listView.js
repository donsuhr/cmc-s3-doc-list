import Backbone from 'backbone';
import ItemView from './itemView';

export default Backbone.View.extend({
    initialize() {
        // first load
        this.listenTo(this.model.get('filteredCollection'), 'sync reset', this.render);
        // set by filter update
        this.listenTo(this.model, 'change:filteredCollection', this.render);
    },

    render(event) {
        const items = this.model.get('filteredCollection');
        this.$el.empty();
        items.each((item) => {
            const view = new ItemView({ model: item });
            this.$el.append(view.render().el);
        });
        return this;
    },
});
