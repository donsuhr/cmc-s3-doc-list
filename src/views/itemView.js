import Backbone from 'backbone';
import template from '../templates/listItemView.hbs';

export default Backbone.View.extend({
    tagName: 'li',
    render() {
        this.$el.html(template(this.model.toJSON()));
        return this;
    },
});
