const webpack = require('webpack');
const path = require('path');

const production = process.env.NODE_ENV === 'production';
const modulesDir = path.resolve(require.resolve('cmc-load-dot-env'), '../..');

const entry = {
    'cmc-s3-doc-list': [path.join(__dirname, 'src/main')],
    page: [path.join(__dirname, '/dev-assets/src/page')],
};

const plugins = [
    new webpack.ProvidePlugin({
        'window.jQuery': 'jquery',
        jQuery: 'jquery',
    }),
];

module.exports = {
    devtool: production ? 'source-map' : '#eval-source-map',
    entry,
    plugins,
    mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
    resolve: {
        alias: {
            config$: path.join(__dirname, 'config.js'),
            underscore: path.join(modulesDir, 'underscore/underscore'),
            handlebars: 'handlebars/runtime',
        },
    },
    output: {
        path: path.join(__dirname, 'dist/'),
        // filename: production ? 'scripts/[name]-[hash].js' : 'scripts/[name].js',
        filename: 'scripts/[name].js',
        // chunkFilename: production ? 'scripts/[name]-[chunkhash].js' : 'scripts/[name].js',
        chunkFilename: 'scripts/[name].js',
        publicPath: '/',
    },
    serve: {
        add: (app, middleware, options) => {
            // reverse order so page loads from webpack, not from static folder
            middleware.webpack();
            middleware.content();
        },
        clipboard: false,
        content: path.resolve(__dirname, 'dev-assets'),
    },
    optimization: {
        runtimeChunk: {
            name: 'cmc-s3-doc-list',
        },
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: ['babel-loader'],
                include: [
                    // path.resolve('.', 'src'),
                    //path.resolve('.', 'config.js'),
                    // /\/node_modules\/cmc-/,
                    //path.resolve('.', 'scripts/page--s3-doc-list.js'),
                ],
            },
            {
                test: /\.hbs$/,
                use: [
                    {
                        loader: 'handlebars-loader',
                        query: {
                            debug: false,
                            runtime: 'handlebars/runtime',
                            // helperDirs: [`${__dirname}/app/html/helpers`],
                            // partialDirs: [
                            //     `${__dirname}/app/html/partials`,
                            // ],
                        },
                    },
                ],
                include: [
                    path.join(__dirname, '/src/'),
                    path.join(__dirname, '/dev-assets/'),
                    /\/node_modules\/cmc-/,
                ],
            },
            {
                test: /\.scss/,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
            { test: /\.css$/, use: ['style-loader', 'css-loader'] },
            {
                test: /\.png$/,
                use: [{ loader: 'url-loader', query: { limit: 100000 } }],
            },
            { test: /\.jpg$/, loader: 'file-loader' },
        ],
    },
};
