import { beforeEach, afterEach, describe, it } from 'mocha';
import { expect } from 'chai';
import nock from 'nock';
import config from 'config';
import $ from 'jquery';
import AppView from '../src/views/appView';
import Router from '../src/router';
import productSheetData from './fixtures/product-sheets.json';

const router = new Router();

describe('s3-doc-list', () => {
    describe('appView', () => {
        beforeEach(() => {
            nock.disableNetConnect();
            nock(`${config.domains.api}/`)
                .get('/docType/site/group/')
                .reply(200, productSheetData);

            document.body.innerHTML = `
                <section id="s3docListApp">
                    <div class="loading--placeholder">Loading...</div>
                </section>
            `;
        });

        afterEach(() => {
            nock.cleanAll();
        });

        it('renders', (done) => {
            new AppView({ // eslint-disable-line no-new
                el: $('#s3docListApp'),
                router,
                docType: 'docType',
                site: 'site',
                group: 'group',
            });
            setTimeout(() => {
                expect(document.querySelector('.s3-doc-list')).not.to.a('null');
                done();
            }, 10);
        });

        it('works with preload data', (done) => {
            new AppView({ // eslint-disable-line no-new
                el: $('#s3docListApp'),
                router,
                docType: 'docType',
                site: 'site',
                group: 'group',
                preloadData: productSheetData.data,
            });
            setTimeout(() => {
                expect(
                    document.querySelectorAll('.s3-doc-list li').length,
                ).to.equal(productSheetData.data.length);
                done();
            }, 20);
        });
    });
});
