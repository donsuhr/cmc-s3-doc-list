/* eslint-env mocha */
const { expect } = require('chai');
const Sut = require('../src/filter').default;

describe('s3-doc-list', () => {
    describe('filter', () => {
        it('updates the filter state', () => {
            const sut = new Sut();
            sut.setState({ Product: ' foo ' });
            expect(sut.get('Product')).to.equal('foo');
        });
    });
});
