import { beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import ItemView from '../src/views/itemView';

let sut;

const item = {
    toJSON() {
        return {
            fullUrl: 'fullUrl',
            title: 'title',
            description: 'description',
        };
    },
};

describe('s3-doc-list', () => {
    describe('itemView', () => {
        beforeEach(() => {
            document.body.innerHTML = `
                <ul class="s3-doc-list"></ul>
            `;
            sut = new ItemView({ model: item });
        });

        it('renders', () => {
            sut.render();
            expect(sut.$el.find('a').text()).to.equal('title');
            expect(sut.$el.find('p').text()).to.equal('description');
        });
    });
});
