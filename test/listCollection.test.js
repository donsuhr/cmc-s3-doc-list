/* eslint-env mocha */
const { expect } = require('chai');
const Sut = require('../src/listCollection').default;
const nock = require('nock');
const config = require('config').default;
const productSheetData = require('./fixtures/product-sheets.json');

const options = {
    s3BucketPrefix: 's3BucketPrefix/',
    docType: 'docType',
    site: 'site',
    group: 'group',
};

describe('s3-doc-list', () => {
    describe('listCollection', () => {
        beforeEach(() => {
            nock.disableNetConnect();

            nock(`${config.domains.api}/`)
                .get(`/${options.docType}/${options.site}/${options.group}/`)
                .reply(
                    200,
                    productSheetData,
                );
        });

        afterEach(() => {
            nock.cleanAll();
        });

        it('is ordered by priority', (done) => {
            const sut = new Sut(null, options);
            sut.fetch().then(() => {
                let lastPriority = 1;
                sut.each((model) => {
                    expect(model.get('priority') <= lastPriority).to.equal(
                        true,
                    );
                    lastPriority = model.get('priority');
                });
                done();
            });
        });

        it('gets unique products', (done) => {
            const sut = new Sut(null, options);
            sut.fetch().then(() => {
                expect(sut.getUniqueProducts()).to.eql([
                    'CampusNexus CRM',
                    'CampusNexus Finance, HR & Payroll',
                    'CampusNexus Student',
                    'Talisma Fundraising',
                ]);
                done();
            });
        });

        it('returns a new filtered collection', (done) => {
            const sut = new Sut(null, options);
            sut.fetch().then(() => {
                const filtered = sut.applyFilter({
                    attributes: { Product: 'CampusNexus Student' },
                });
                filtered.each((model) => {
                    expect(model.get('products')).to.include(
                        'CampusNexus Student',
                    );
                });
                done();
            });
        });
    });
});
