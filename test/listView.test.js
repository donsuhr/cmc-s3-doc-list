import { beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import $ from 'jquery';
import _ from 'underscore';
import ItemView from '../src/views/listView';

let sut;

const items = [1, 2, 3].map(x => ({
    toJSON() {
        return {
            fullUrl: `fullUrl ${x}`,
            title: `title ${x}`,
            description: `description ${x}`,
        };
    },
}));

const selectedFilterModel = {
    get(prop) {
        const props = {
            filteredCollection: {
                each: _.partial(_.each, items),
            },
        };
        return props[prop];
    },
};

describe('s3-doc-list', () => {
    describe('listView', () => {
        beforeEach(() => {
            document.body.innerHTML = `
                <ul class="s3-doc-list"></ul>
            `;
            sut = new ItemView({
                el: $('.s3-doc-list'),
                model: selectedFilterModel,
            });
        });

        it('renders', () => {
            sut.render();
            expect(
                document.querySelectorAll('.s3-doc-list > li ').length,
            ).to.equal(items.length);
        });
    });
});
