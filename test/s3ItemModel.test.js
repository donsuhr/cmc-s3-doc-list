/* eslint-env mocha */
const { expect } = require('chai');
const Sut = require('../src/s3ItemModel').default;

describe('s3-doc-list', () => {
    describe('s3ItemModel', () => {
        it('adds a fullUrl to a response -- prod', () => {
            const sut = new Sut();
            sut.collection = {
                s3BucketPrefix: 's3BucketPrefix/',
                docType: 'docType',
                site: 'site',
                group: 'group',
            };
            const serverResponse = {
                s3url: 's3url',
            };
            const origNodeEnv = process.env.NODE_ENV;
            process.env.NODE_ENV = 'production';
            let result = sut.parse(serverResponse);
            expect(result.fullUrl).to.match(
                /docs\/docType\/site\/group\/s3url/,
            );
            process.env.NODE_ENV = 'not-production';
            result = sut.parse(serverResponse);
            expect(result.fullUrl).to.match(
                /docType--site--group\.s3\.amazonaws\.com\/s3url/,
            );
            process.env.NODE_ENV = origNodeEnv;
        });
    });
});
