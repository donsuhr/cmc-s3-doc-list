/* eslint-env mocha */
const { expect } = require('chai');
const sinon = require('sinon');
const sut = require('../src/router').default;
const Backbone = require('backbone');

// let clock;
let sandbox;

describe('s3-doc-list', () => {
    describe('router', () => {
        beforeEach(() => {
            sandbox = sinon.sandbox.create();
            document.location.hash = '';
            Backbone.history.start();
        });

        afterEach(() => {
            sandbox.restore();
            Backbone.history.stop();
        });

        it('updates the filter on navigate', (done) => {
            const routerSpy = sandbox.spy(sut.prototype, 'filter');
            new sut(); // eslint-disable-line no-new,new-cap

            document.location.hash = '#filter/Product/foo';
            setTimeout(() => {
                expect(routerSpy.calledWith('foo')).to.equal(true);
                done();
            }, 10);
        });

        it('updates the url on updateFilter', () => {
            const router = new sut(); // eslint-disable-line no-new,new-cap
            router.updateFilter({ attributes: { Product: 'bar' } });
            expect(document.location.hash).to.equal('#filter/Product/bar');
            router.updateFilter({
                attributes: { Product: 'baz', Catagory: '' },
            });
            expect(document.location.hash).to.equal('#filter/Product/baz');
        });
    });
});
