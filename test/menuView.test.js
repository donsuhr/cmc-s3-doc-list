import { beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import sinon from 'sinon';
import $ from 'jquery';
import MenuView from '../src/views/menuView';

let sut;

const filter = {
    val: 'two',
    set() {},
    get() {
        return this.val;
    },
};

const menuListCollection = {
    get(prop) {
        const props = {
            allItemsCollection: {
                getUniqueProducts() {
                    return ['one', 'two', 'three'];
                },
            },
            filter,
        };
        return props[prop];
    },
};

describe('s3-doc-list', () => {
    describe('menuView', () => {
        beforeEach(() => {
            document.body.innerHTML = `
                <ul class="s3-doc-list__menu sf-menu horizontal-menu"></ul>
            `;
            sut = new MenuView({
                el: $('.s3-doc-list__menu'),
                model: menuListCollection,
                menuText: 'menu text',
            });
        });

        it('renders', () => {
            expect(
                document.querySelector('.s3-doc-list__menu > li > a'),
            ).not.to.a('null');
            filter.val = '';
            sut.updateMenuItemData();
            expect(
                document.querySelector('.s3-doc-list__menu > li > a')
                    .textContent,
            ).to.equal('menu text');
        });

        it('updates menu item data', () => {
            filter.val = 'two';
            sut.updateMenuItemData();
            expect(
                document.querySelector('.s3-doc-list__menu > li > a')
                    .textContent,
            ).to.equal('two');
        });

        it('changes the menu', () => {
            const spy = sinon.spy(filter, 'set');
            const newFilter = {
                property: 'Product',
                value: 'three',
            };
            sut.onMenuChange(newFilter);
            expect(
                spy.calledWith(newFilter.property, newFilter.value),
            ).to.equal(true);
        });

        it('shows default text', () => {
            document.body.innerHTML = `
                <ul class="s3-doc-list__menu sf-menu horizontal-menu"></ul>
            `;
            sut = new MenuView({
                el: $('.s3-doc-list__menu'),
                model: menuListCollection,
            });
            filter.val = '';
            sut.updateMenuItemData();
            expect(
                document.querySelector('.s3-doc-list__menu > li > a')
                    .textContent,
            ).to.equal('Select Product');
        });

        it('resets', () => {
            sut.updateMenuItemData();
            expect(
                document.querySelectorAll('.s3-doc-list__menu > li > ul > li')
                    .length,
            ).to.equal(4);
            sut.renderReset();
            expect(
                document.querySelectorAll('.s3-doc-list__menu > li > ul > li')
                    .length,
            ).to.equal(1);
        });
    });
});
