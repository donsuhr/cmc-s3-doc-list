const nodeExternals = require('webpack-node-externals'); // eslint-disable-line import/no-extraneous-dependencies
const path = require('path');

const isCoverage = process.env.NODE_ENV === 'coverage';

module.exports = {
    target: 'node', // in order to ignore built-in modules like path, fs, etc.
    mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
    externals: [
        nodeExternals(
            // in order to ignore all modules in node_modules folder
            {
                // this WILL include items
                whitelist: [
                    'cmc-loading-view',
                    'cmc-selected-filter-view',
                    'cmc-horizontal-menu-item-view',
                ],
            },
        ),
    ],
    optimization: {
        nodeEnv: false
    },
    devtool: 'inline-cheap-module-source-map',
    resolve: {
        // modules: ['node_modules', path.resolve(__dirname, 'app')],
        alias: {
            config: path.join(__dirname, 'config.js'),
            underscore: path.join(
                __dirname,
                'node_modules/underscore/underscore',
            ),
            handlebars: 'handlebars/runtime',
        },
    },
    plugins: [
        // new NodePathReplacePlugin(),
    ],
    output: {
        // use absolute paths in sourcemaps (important for debugging via IDE)
        devtoolModuleFilenameTemplate: '[absolute-resource-path]',
        devtoolFallbackModuleFilenameTemplate:
            '[absolute-resource-path]?[hash]',
    },
    module: {
        rules: [].concat(
            isCoverage
                ? {
                    test: /\.js$/,
                    include: path.join(__dirname, '/app/'), // instrument only testing sources with Istanbul, after ts-loader runs
                    exclude: [/test.js$/, /scripts\/vendor/],
                    loader: 'istanbul-instrumenter-loader',
                    query: {
                        esModules: true,
                    },
                }
                : [],
            {
                test: /\.js$/,
                use: ['babel-loader'],
                include: [
                    path.join(__dirname, '/src/'),
                    path.join(__dirname, '/config.js'),
                    path.join(__dirname, '/node_modules/cmc-loading-view'),
                    path.join(
                        __dirname,
                        '/node_modules/cmc-horizontal-menu-item-view',
                    ),
                    path.join(
                        __dirname,
                        '/node_modules/cmc-selected-filter-view',
                    ),
                ],
            },
            {
                test: /\.hbs$/,
                use: [
                    {
                        loader: 'handlebars-loader',
                        query: {
                            debug: false,
                            runtime: 'handlebars/runtime',
                            helperDirs: [`${__dirname}/app/metalsmith/helpers`],
                            partialDirs: [
                                `${__dirname}/app/metalsmith/partials`,
                            ],
                        },
                    },
                ],
                include: path.join(__dirname, '/src/'),
            },
        ),
    },
};
